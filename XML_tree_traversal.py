# -*- coding: UTF-8 -*-
# 从文件中读取数据
import xml.etree.ElementTree as ET
import numpy as np
import copy

# 全局唯一标识
unique_id = 0


# 遍历所有的节点
def walkData(root_node, level, result_list, parent_node):
    global unique_id

    if '}' in root_node.tag:
        root_node_text = root_node.tag.split('}')[-1]
    else:
        root_node_text = str(root_node.tag)

    if root_node.text is not None:
        root_node_value = root_node.text
    else:
        root_node_value = ''
    temp_list = [unique_id, level, root_node_text, root_node.attrib, root_node_value, parent_node]
    result_list.append(temp_list)
    father_node = copy.deepcopy(unique_id)
    unique_id += 1

    # 遍历每个子节点
    children_node = root_node.getchildren()
    if len(children_node) == 0:
        return
    for child in children_node:
        walkData(child, level + 1, result_list, father_node)
    return


# 获得原始数据
# out:
# [
#    #ID, Level, Attr Map
#    [1, 1, {'ID':1, 'Name':'test1'}],
#    [2, 1, {'ID':1, 'Name':'test2'}],
# ]
def getXmlData(file_name):
    level = 1  # 节点的深度从1开始
    result_list = []
    root = ET.parse(file_name).getroot()
    walkData(root, level, result_list, -1)

    return result_list

def get_XML_regulartext(file_name):
    # file_name="BW_93825.xml"

    xml_name = file_name.split('.')[0]

    source=''
    R = getXmlData(source+file_name)

    #     for x in R:
    #         print(x)
    #     pass

    Feature_Dict = {}

    for x in range(len(R)):
        unique_id = R[x][0]
        level = R[x][1]
        root_node_text = R[x][2]
        root_node_attrib = R[x][3]
        root_node_value = R[x][4]
        parent_node = R[x][5]

        if len(root_node_attrib.keys()) == 0:
            if x + 1 < len(R):
                if R[x + 1][1] <= level:
                    origin_text = copy.deepcopy(root_node_text)

                    while parent_node > -1:
                        origin_text = R[parent_node][2] + '.' + origin_text
                        parent_node = R[parent_node][5]
                    if origin_text not in Feature_Dict.keys():
                        Feature_Dict.setdefault(origin_text)
                        Feature_Dict[origin_text] = root_node_value

        else:
            for item in root_node_attrib.keys():

                origin_text = root_node_text + '.' + str(item)
                parent_node_x = copy.deepcopy(parent_node)
                while parent_node_x > -1:
                    origin_text = R[parent_node_x][2] + '.' + origin_text
                    parent_node_x = R[parent_node_x][5]

                if origin_text not in Feature_Dict.keys():
                    Feature_Dict.setdefault(origin_text)
                    Feature_Dict[origin_text] = root_node_attrib[item]

    XML_HotelTags = xml_name + ' XML_HotelTags.txt'
    f_XML = open(source+XML_HotelTags, 'w')  # 若是'wb'就表示写二进制文件

    unique_id = 1
    for x in Feature_Dict:
        string_list = str(x).split('.')
        keep_length = round(len(string_list) * 0.25)
        x_text = '.'.join(string_list[-keep_length:])
        #         print(x,'*',Feature_Dict[x])

        print_text = x_text + '*' + Feature_Dict[x]
        write_text = str(unique_id) + ' #' + x + '*' + Feature_Dict[x]+'¥'
        print(str(unique_id), x_text, '*', Feature_Dict[x])

        f_XML.write(write_text)
        f_XML.write('\n')

        unique_id = unique_id + 1

    print("There are {} items need mapping".format(len(Feature_Dict)))
    f_XML.close()

    return XML_HotelTags

# # file_name = 'text.xml'
# # file_name = 'HILTON_XMNWB.xml'
# # file_name="ACCOR_0351.xml"
# file_name="BW_93825.xml"
#
# # get_XML_regulartext(file_name)