#!/usr/bin/env python
# -*- coding: utf-8 -*-


from XML_tree_traversal import get_XML_regulartext
from xml_wordpredict import Get_contain_nes,return_percentile,all_list,get_WordPredict
from xml_dnnpredict import get_DNNPredict
import numpy as np
import pickle,pprint
from collections import Counter
import xlwt


# file_name="ACCOR_0351.xml"
# file_name = 'HILTON_XMNWB.xml'
file_name = "BW_93825.xml"


#region Read GHR_Attribute_Pkl

meta_path='./checkpoint_dir/MyModel/model.meta'
checkpoint_dir='./checkpoint_dir/MyModel'

pickle_path='GHR_Attribute.pkl'

pkl_file = open(pickle_path,'rb')       #打开保存的pickle文件

#按照pickle中存放的位置，顺序打印

GHR_Attribute_Dict = pickle.load(pkl_file)       #打印出字典

#endregion

#region GHRKeyword
GHRKeyword_file_name='GHRKeyword.txt'

with open(GHRKeyword_file_name,'r',encoding='UTF-8') as f:
    GHR_Keyword = f.read()

GHR_Keyword_List=GHR_Keyword.lower().split('\n')
#endregion


file_name=get_XML_regulartext(file_name)

with open(file_name,'r',encoding='UTF-8') as f:
    XML_IntegrateData = f.read()

# XML_IntegrateData_List=XML_IntegrateData.split('\n')
XML_IntegrateData_List=XML_IntegrateData.split('¥')

#region 获取关键词的重要性
contain_keyword_list=[]
for line in XML_IntegrateData_List:
    line=line.strip()
    XML_IntegrateData_Text=line.split('#')[-1]
    XML_IntegrateData_Content=XML_IntegrateData_Text.split('*')[0]
    XML_IntegrateData_Value=XML_IntegrateData_Text.split('*')[-1]

    contain_keyword, nes_keyword=Get_contain_nes(GHR_Keyword_List,XML_IntegrateData_Content)

    contain_keyword_list.extend(list(contain_keyword))

contain_keyword_counter=all_list(contain_keyword_list)          #获取关键词的重要性
#endregion

XMLMapping_file_name=file_name.split(' ')[0]
XML_Mapping_TXT= open(XMLMapping_file_name+'_MappingResult.txt', 'w')  # 若是'wb'就表示写二进制文件

book=xlwt.Workbook(encoding='utf-8',style_compression=0)
sheet=book.add_sheet('mapping-result',cell_overwrite_ok=True)


# txt1='tag-name'
start_point=0
len_j=12

item_id=0

for line in XML_IntegrateData_List:
    # line='Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.AreaInfo.Attractions.Attraction.ID'
    line=line.strip()
    XML_IntegrateData_Text=line.split('#')[-1]
    XML_IntegrateData_Content=XML_IntegrateData_Text.split('*')[0]
    XML_IntegrateData_Value=XML_IntegrateData_Text.split('*')[-1]

    contain_keyword, nes_keyword=Get_contain_nes(GHR_Keyword_List,XML_IntegrateData_Content)

    contain_keyword_count=np.sum([contain_keyword_counter[x] for x in contain_keyword])

    contain_keyword_count_dict, GHR_Attribute_Score_Dict=get_WordPredict(GHR_Attribute_Dict, contain_keyword_counter,
                                                                         contain_keyword,nes_keyword,
                                                                         contain_keyword_count)

    input_word_origin=str(XML_IntegrateData_Value)
    Dnn_Attribute_Score_Dict = get_DNNPredict(input_word_origin, meta_path, checkpoint_dir)

    Integrated_GHR_Attribute_Score_Dict={}
    for GHR_Attribute_Item in GHR_Attribute_Dict:
        Integrated_GHR_Attribute_Score_Dict.setdefault(GHR_Attribute_Item)
        Integrated_GHR_Attribute_Score_Dict[GHR_Attribute_Item]=(0.3*Dnn_Attribute_Score_Dict[GHR_Attribute_Item]+0.7*GHR_Attribute_Score_Dict[GHR_Attribute_Item])



    print("** " + str(item_id), XML_IntegrateData_Content)
    XML_Mapping_TXT.write("** " + str(item_id) + ' ' + XML_IntegrateData_Content)
    XML_Mapping_TXT.write('\n')

    sheet.write(start_point,0,int(item_id))

    sheet.write(start_point,1,list(set([XML_IntegrateData_Content.split('.')[-1]]))[0])
    sheet.write(start_point+1,1,"Description: " +XML_IntegrateData_Content)

    keyword_mostcommon = Counter(contain_keyword_count_dict).most_common(3)

    etc = ''
    if len(XML_IntegrateData_Value) > 50:
        etc = '...'

    print("Value: " + XML_IntegrateData_Value[:50] + etc)

    XML_Mapping_TXT.write("Value: " + XML_IntegrateData_Value[:50] + etc)
    XML_Mapping_TXT.write('\n')

    sheet.write(start_point+2,1,"Value: " + XML_IntegrateData_Value[:50] + etc)


    print("Keyword: ", keyword_mostcommon, "Important: ", set([XML_IntegrateData_Content.split('.')[-1]]))

    XML_Mapping_TXT.write("Keyword: ")

    for common_i in keyword_mostcommon:
        XML_Mapping_TXT.write('(' + str(common_i[0]) + ',' + str(common_i[1]) + ')')

    XML_Mapping_TXT.write('\n')

    XML_Mapping_TXT.write("Important: " + list(set([XML_IntegrateData_Content.split('.')[-1]]))[0])

    XML_Mapping_TXT.write('\n')

    # print("Candidate :",Counter(GHR_Attribute_Score_Dict).most_common(12))
    i = 0
    start_point_j=0
    for item in Counter(Integrated_GHR_Attribute_Score_Dict).most_common(12):
        print(str(i) + ': ', item)

        XML_Mapping_TXT.write(str(i) + ': ' + str(item))
        XML_Mapping_TXT.write('\n')

        sheet.write(start_point + start_point_j, 2, str(i) + ': ' + str(item))
        start_point_j=start_point_j+1

        i = i + 1



    print('\n')
    XML_Mapping_TXT.write('\n')

    a = 1
    item_id = item_id + 1

    start_point=start_point+len_j

    # XML_Mapping_TXT.write(str(i))
    # XML_Mapping_TXT.write('\n')

XML_Mapping_TXT.close()

book.save(XMLMapping_file_name+'_MappingResult.xls')

