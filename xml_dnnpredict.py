#!/usr/bin/env python
# -*- coding: utf-8 -*-

import warnings
warnings.filterwarnings("ignore")

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import tensorflow as tf
from collections import Counter
import pickle,pprint


def source_to_seq(text):
    '''
    对源数据进行转换
    '''
    sequence_length_max = 53

    original_text = [source_letter_to_int.get(word, source_letter_to_int['<UNK>']) for word in text]
    if len(original_text) > sequence_length_max:
        original_text = original_text[:sequence_length_max]

    return original_text

def get_DNNPredict(input_word_origin, meta_path, checkpoint_dir):
    input_word = input_word_origin.lower()
    if len(input_word) > 50:
        input_word = input_word[:50] + '...'

    input_word = input_word.replace('\n', '')

    text = source_to_seq(input_word)  # 把输入转成小写
    loaded_graph = tf.Graph()

    with tf.Session(graph=loaded_graph) as sess:

        loader = tf.train.import_meta_graph(meta_path)
        loader.restore(sess, tf.train.latest_checkpoint(checkpoint_dir))

        inputs = loaded_graph.get_tensor_by_name('dnn/placeholders/inputs:0')
        outputs = loaded_graph.get_tensor_by_name('dnn/model/outputs:0')
        answer_logits = loaded_graph.get_tensor_by_name('dnn/evaluation/answer_logits:0')

        answer_logits, logit_prob = sess.run([answer_logits, outputs], feed_dict={inputs: [text]})

    # print('原始输入:', input_word)
    #
    # print('\nSource')
    # print('  Word 编号:    {}'.format([i for i in text]))
    # print('  Input Words: {}'.format(" ".join([source_int_to_letter[i] for i in text])))
    #
    # attribute_index = int(answer_logits[0])
    # answer = list(GHR_Attribute_Dict.keys())[attribute_index]
    # print('  GHR Attribute:    {}'.format(answer))
    # print('\n')

    # logitprob_bestindex = list(np.argsort(logit_prob[0])[-10:])
    # logit_p = logit_prob[0][logitprob_bestindex]
    #
    # for i in logitprob_bestindex:
    #     #   attribute_index=int(answer_logits[0])
    #     answer = list(GHR_Attribute_Dict.keys())[i]
    #     print('  GHR Attribute:    {}'.format(answer), 'probability:  {}'.format(logit_p[logitprob_bestindex.index(i)]))

    GHR_Attribute_Score_Dict={}
    q=0
    for GHR_Attribute_Item in GHR_Attribute_Dict:

        GHR_Attribute_Score_Dict.setdefault(GHR_Attribute_Item)
        GHR_Attribute_Score_Dict[GHR_Attribute_Item]=logit_prob[0][q]

        q=q+1

    return GHR_Attribute_Score_Dict


pkl_file = open('GHR_Attribute_Dict.pkl','rb')       #打开保存的pickle文件

#按照pickle中存放的位置，顺序打印

GHR_Attribute_Dict = pickle.load(pkl_file)       #打印出字典

pkl_file = open('source_vocab.pkl','rb')       #打开保存的pickle文件
source_int_to_letter = pickle.load(pkl_file)       #打印出字典
source_letter_to_int = pickle.load(pkl_file)       #打印出字典




# input_word_origin='dalae_ds@hilton.com'
# input_word_origin='https://media.iceportal.com/45477/photos/0039714_XL.jpg'
# input_word_origin='92-44-5191'
# input_word_origin='0.00'
# input_word_origin='Best Western Heritage Inn'
# input_word_origin='Comfort and convenience was our priority when we designed'
# input_word_origin='us'
# input_word_origin='cn'
# input_word_origin='05560'


# meta_path='./checkpoint_dir/MyModel/model.meta'
# checkpoint_dir='./checkpoint_dir/MyModel'
#
#
# attribute_probability=get_DNNPredict(input_word_origin, meta_path, checkpoint_dir)
#
# print(attribute_probability)
# a=1








