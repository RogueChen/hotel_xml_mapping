#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pickle,pprint
import numpy as np
from collections import Counter

def Get_contain_nes(GHR_Keyword_List,IntegrateData_Content):
    test_content = IntegrateData_Content.lower()
    contain_keyword = []
    nes_keyword = []
    for keyword in GHR_Keyword_List:
        if len(keyword)>0 and keyword != 'en-us':
            if keyword in test_content:
                contain_keyword.append(keyword)

            if keyword in test_content.split('.')[-1]:
                nes_keyword.append(keyword)
    if len(nes_keyword)==0 and test_content.split('.')[-1]=='en-us':
        nes_keyword.append(test_content.split('.')[-2])

    if len(nes_keyword)==0:
        print('***********************')

    return set(contain_keyword),set(nes_keyword)

def return_percentile(a_temp):
    a=np.array(a_temp)
    new_a=[]
    for i in range(len(a)):
        b=len(a[a<a[i]])/len(a)
        # print(b)
        new_a.append(b)

    # print(new_a)

    return new_a

def all_list(arr):
    result = {}
    for i in set(arr):
        result[i] = 1/arr.count(i)
    return result

def get_WordPredict(GHR_Attribute_Dict, contain_keyword_counter,
                                 contain_keyword,nes_keyword,
                                 contain_keyword_count):

    score_list=[]
    GHR_Attribute_Score_Dict={}
    for GHR_Attribute_Item in GHR_Attribute_Dict:

        if len(GHR_Attribute_Item)>0:
            GHR_Attribute_Item_contain_keyword=GHR_Attribute_Dict[GHR_Attribute_Item][0]
            GHR_Attribute_Item_nes_keyword=GHR_Attribute_Dict[GHR_Attribute_Item][1]

            part1=GHR_Attribute_Item_nes_keyword==nes_keyword

            part2=0
            contain_keyword_count_dict={}
            if contain_keyword_count>0:
                for keyword_i in contain_keyword:
                    contain_keyword_count_dict[keyword_i]=contain_keyword_counter[keyword_i]/contain_keyword_count

                    part2added=(contain_keyword_counter[keyword_i]/contain_keyword_count)*(keyword_i in GHR_Attribute_Item_contain_keyword)
                    part2=part2+part2added

            part3=len(GHR_Attribute_Item_contain_keyword&contain_keyword)/len(GHR_Attribute_Item_contain_keyword)


            # part1=len(contain_keyword & GHR_Attribute_Item_nes_keyword)/len(GHR_Attribute_Item_nes_keyword)
            total_score=0.4*part1+0.3*part2+0.3*part3

            # total_score=100-total_score

            score_list.append(total_score)

            GHR_Attribute_Score_Dict.setdefault(GHR_Attribute_Item)
            GHR_Attribute_Score_Dict[GHR_Attribute_Item]=total_score


    return contain_keyword_count_dict,GHR_Attribute_Score_Dict




















