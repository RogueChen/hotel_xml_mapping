1 #Envelope.Header*¥
2 #Envelope.Body.OTA_HotelDescriptiveInfoRS.Target*Production¥
3 #Envelope.Body.OTA_HotelDescriptiveInfoRS.PrimaryLangID*EN-US¥
4 #Envelope.Body.OTA_HotelDescriptiveInfoRS.Version*4.0¥
5 #Envelope.Body.OTA_HotelDescriptiveInfoRS.TimeStamp*2018-12-18T10:47:47.047¥
6 #Envelope.Body.OTA_HotelDescriptiveInfoRS.Success*¥
7 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.HotelName*Best Western Grand Hotel Le Touquet¥
8 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.ChainCode*BW¥
9 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.HotelCode*93825¥
10 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.HotelCodeContext*BESTWESTERN¥
11 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.LanguageCode*EN-US¥
12 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.BrandCode*BEST¥
13 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.CurrencyCode*EUR¥
14 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.HotelInfo.HotelName*Best Western Grand Hotel Le Touquet¥
15 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.HotelInfo.Descriptions.DescriptiveText*The Best Western Grand Hotel Le Touquet is a trendy 4 Star hotel. It has 116 stylish rooms with many benefits from stunning views over the Cote d’Opale. 

The hotel is located in the heart of a charming forest, but still provides easy access to Le Touquets considerable attractions.

Two restaurants and a bar: take your time to enjoy the finest cuisine at the Best Western Grand Hotel Le Touquet. The bar has a lounge area which will host you for a perfect beginning just before you go to La Lorelei, a restaurant which offers an intimate atmosphere or to The Roots restaurant.

The relaxing area at the Best Western Grand Hotel Le Touquet is fully dedicated to your well-being. The Thalgo® Spa Center is waiting for your serenity special moment. The inside pool has a warm 28 degree Celsius water temperature and a sauna on free access.

The Best Western Grand Hotel Le Touquet is another pleasant way to enjoy the famous seaside town of the four seasons, to plan, all year long, your stays or events and invite your family, friends, coworkers and customers to take a bowl of fresh air before going to new horizons.¥
16 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.HotelInfo.Position.Latitude*50.526834¥
17 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.HotelInfo.Position.Longitude*1.596451¥
18 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.HotelInfo.Services.Service.CodeDetail*Smoke-free property¥
19 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.HotelInfo.Services.Service.Code*312¥
20 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.HotelInfo.CategoryCodes.SegmentCategory.Code*16¥
21 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.HotelInfo.CategoryCodes.SegmentCategory.CodeDetail*Best Western¥
22 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.HotelInfo.CategoryCodes.GuestRoomInfo.Code*5¥
23 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.HotelInfo.CategoryCodes.GuestRoomInfo.CodeDetail*Floors¥
24 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.HotelInfo.CategoryCodes.GuestRoomInfo.Quantity*5¥
25 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.FacilityInfo.GuestRooms.GuestRoom.MaxOccupancy*2¥
26 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.FacilityInfo.GuestRooms.GuestRoom.ID*2281166¥
27 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.FacilityInfo.GuestRooms.GuestRoom.RoomTypeName*1 Double Bed, Non-Smoking, Standard Room, Cozy Room¥
28 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.FacilityInfo.GuestRooms.GuestRoom.TypeRoom.RoomTypeCode*REY¥
29 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.FacilityInfo.GuestRooms.GuestRoom.Amenities.Amenity.RoomAmenityCode*74¥
30 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.FacilityInfo.GuestRooms.GuestRoom.Amenities.Amenity.CodeDetail*Non-smoking¥
31 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.FacilityInfo.MeetingRooms.TotalRoomSeatingCapacity*200¥
32 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.FacilityInfo.MeetingRooms.MeetingRoom.MultimediaDescriptions.MultimediaDescription.ImageItems.ImageItem.Category*8¥
33 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.FacilityInfo.MeetingRooms.MeetingRoom.MultimediaDescriptions.MultimediaDescription.ImageItems.ImageItem.ImageFormat.Width*390¥
34 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.FacilityInfo.MeetingRooms.MeetingRoom.MultimediaDescriptions.MultimediaDescription.ImageItems.ImageItem.ImageFormat.Height*260¥
35 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.FacilityInfo.MeetingRooms.MeetingRoom.MultimediaDescriptions.MultimediaDescription.ImageItems.ImageItem.ImageFormat.URL*https://media.iceportal.com/67035/photos/0615009_M.jpg¥
36 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.FacilityInfo.MeetingRooms.MeetingRoom.MultimediaDescriptions.MultimediaDescription.ImageItems.ImageItem.Description.Caption*Meeting Room¥
37 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.Policies.Policy.PolicyInfo.CheckInTime*15:00:00¥
38 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.Policies.Policy.PolicyInfo.CheckOutTime*12:00:00¥
39 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.Policies.Policy.PolicyInfo.UsualStayFreeCutOffAge*12¥
40 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.Policies.Policy.CancelPolicy*¥
41 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.Policies.Policy.GuaranteePaymentPolicy.GuaranteePayment.AcceptedPayments.AcceptedPayment.PaymentCard.CardType.Code*AX¥
42 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.Policies.Policy.TaxPolicies.TaxPolicy.Type*Inclusive¥
43 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.Policies.Policy.TaxPolicies.TaxPolicy.Code*19¥
44 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.Policies.Policy.TaxPolicies.TaxPolicy.CurrecyCode*EUR¥
45 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.Policies.Policy.TaxPolicies.TaxPolicy.Percent*10¥
46 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.Policies.Policy.TaxPolicies.TaxPolicy.EffectiveDate*2015-08-06¥
47 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.Policies.Policy.TaxPolicies.TaxPolicy.ChargeUnit*25¥
48 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.Policies.Policy.TaxPolicies.TaxPolicy.ChargeFrequency*1¥
49 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.Policies.Policy.TaxPolicies.TaxPolicy.Amount*2.2¥
50 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.Policies.Policy.PetsPolicies.PetsPolicy.Description.Text*Pets allowed with Restrictions, pets are not allowed in restaurants and spa area¥
51 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.AreaInfo.Recreations.Recreation.CodeDetail*Steam bath¥
52 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.AreaInfo.Recreations.Recreation.Code*93¥
53 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.AreaInfo.Recreations.Recreation.DescriptiveText*Steam room¥
54 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.AreaInfo.RefPoints.RefPoint.Distance*17¥
55 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.AreaInfo.RefPoints.RefPoint.RefPointCategoryCode*37¥
56 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.AreaInfo.RefPoints.RefPoint.RefPointName*MONTREUIL SUR MER, FRANCE, PAS DE CALAIS¥
57 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.AreaInfo.RefPoints.RefPoint.DescriptiveText*17 kilometer(s) from Montreuil Sur Mer, France, Pas de Calais¥
58 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.AreaInfo.RefPoints.RefPoint.UnitOfMeasureCode*1¥
59 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.AreaInfo.Attractions.Attraction.DescriptiveText*2 kilometer(s) from Aqualud, Aquatic center for all family, with restaurant¥
60 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.AreaInfo.Attractions.Attraction.AttractionCategoryCode*44¥
61 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.AreaInfo.Attractions.Attraction.AttractionName*RUE SAINT JEAN, MAIN STREET OF THE STATION. 7 DAYS A WEEK, ALL YEAR LONG, FIND MORE THAN 100 SHOPS RUE SAINT JEAN AND AROUND.¥
62 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.AffiliationInfo.Awards.Award.Rating*4 Star¥
63 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.MultimediaDescriptions.MultimediaDescription.TextItems.TextItem.Title*Driving directions¥
64 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.MultimediaDescriptions.MultimediaDescription.TextItems.TextItem.Description*·From: Paris. Take A16 in direction of Amiens.
Take exit 26 Le Touquet Paris Plage. On the roundabout, take on your right Route dHilbert. On the roundabout, take on your left Route dHilbert. In the next roundabout,continue to follow straight away Route dHilbert. On the roundabout, take on your left Route dHilbert
On the roundabout,take on your left Rue du Touquet. On the roundabout, follow straight away Avenue de lEurope. Continue on your right in Avenue Jean de Lattre de Tassigny. Take on your right Avenue du Golf. On the roundabout, go straight in Avenue du Golf
Take on your left Avenue de Picardie. On the roundabout, follow straight Avenue de Picardie. On the roundabout,go straight Avenue de Picardie. Take on your right Avenue du 18 Juin. Take on your left Boulevard de la Canch to the Best Western Grand hotel Le Touquet.

·From: Calais. Follow A16/E402 (Rocade Littorale) in direction of Boulogne, Rouen. Take Exit 26 Le Touquet Paris Plage. On the roundabout, take on your right Route dHilbert. On the roundabout, take on your left Route dHilbert. In the next roundabout, continue to follow straight away Route dHilbert. On the roundabout, take on your left Route dHilbert. On the roundabout, take on your left Rue du Touquet. On the roundabout, follow straight away Avenue de lEurope. Continue on your right in Avenue Jean de Lattre de Tassigny. Take on your right Avenue du Golf. On the roundabout, go straight in Avenue du Golf. Take on your left Avenue de Picardie. On the roundabout, follow straight Avenue de Picardie. On the roundabout, go straight Avenue de Picardie. Take on your right Avenue du 18 Juin. The Best Western Grand Hotel Le Touquet in on the left.

·From: Lille. Take A25/E42 to Dunkerque, Bethune
then follow A16 in direction of Calais. Take Exit 26 Le Touquet Paris Plage. On the roundabout, take on your right Route dHilbert. On the roundabout, take on your left Route dHilbert. In the next roundabout, continue to follow straight away on Route dHilbert. On the roundabout, take on your left Route dHilbert. On the next roundabout, take on your left Rue du Touquet. On the roundabout, follow straight away on Avenue de lEurope. Continue on your right in Avenue Jean de Lattre de Tassigny. Take on your right Avenue du Golf. On the roundabout, go straight in Avenue du Golf. Take on your left om Avenue de Picardie. On the roundabout, follow straight Avenue de Picardie. On the roundabout, go straight Avenue de Picardie. Take on your right Avenue du 18 Juin. The Best Western Grand Hotel Le Touquet is on the left.¥
65 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.ContactInfos.ContactInfo.Addresses.Address.AddressLine*4 boulevard de la Canche¥
66 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.ContactInfos.ContactInfo.Addresses.Address.CityName*Le Touquet¥
67 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.ContactInfos.ContactInfo.Addresses.Address.PostalCode*62520¥
68 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.ContactInfos.ContactInfo.Addresses.Address.CountryName.Code*FR¥
69 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.ContactInfos.ContactInfo.Phones.Phone.PhoneNumber*+33 3 21 06 88 88¥
70 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.ContactInfos.ContactInfo.Phones.Phone.PhoneTechType*1¥
71 #Envelope.Body.OTA_HotelDescriptiveInfoRS.HotelDescriptiveContents.HotelDescriptiveContent.ContactInfos.ContactInfo.Phones.Phone.PhoneLocationType*7¥
